import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import mongoose from 'mongoose';
import { Server } from 'socket.io';

import './configs/dotenvConfig.js';
import { authRouter, todoRouter, userRouter, viewRouter } from './routes/index.js';
import { serverConfig } from './configs/serverConfig.js';
import { globalErrorHandler } from './controllers/errorController.js';

const app = express();

mongoose
  .connect(serverConfig.mongoUrl)
  .then(() => {
    console.log('Mongo DB connected..');
  })
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });

// MIDDLEWARES ===========================
if (process.env.NODE_ENV === 'development') app.use(morgan('dev'));
// app.use(morgan('dev'));

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

// Setup PUG template engine
app.set('view engine', 'pug');
app.set('views', 'views');

// global custom middleware
app.use((req, res, next) => {
  console.log('Hello from middleware!!!!');

  req.time = new Date().toLocaleString('uk-UA');

  next();
});

// CONTROLLERS ===========================
app.get('/ping', (req, res) => {
  // res.send('<p>Hello from server!!!</p>');
  // res.sendStatus(201);
  res.status(200).json({
    msg: 'pong!!!',
  });
});

// ROUTES ====================
const pathPrefix = '/api/v1';

app.use(`${pathPrefix}/auth`, authRouter);
app.use(`${pathPrefix}/todos`, todoRouter);
app.use(`${pathPrefix}/users`, userRouter);
app.use('/', viewRouter);

// Handle not found error
app.all('*', (req, res) => {
  res.status(404).json({
    msg: 'Oops! Resource not found!',
  });
});

// Global error handler
app.use(globalErrorHandler);

// SERVER INIT ===============================
const { port } = serverConfig;

const server = app.listen(port, () => {
  console.log(`Server is up and running on port ${port}`);
});

// SOCKET.IO example ===========================
const io = new Server(server);

// Ex.1 basic ======================
// io.on('connection', (socket) => {
//   console.log('Client connected!');

//   // Emit custom event
//   socket.emit('message', { msg: 'Hello from socket!' });

//   socket.on('custom', (data) => {
//     console.log('>>>>>>>>>>>>>>>>>>>>>>>');
//     console.log({ data });
//     console.log('<<<<<<<<<<<<<<<<<<<<<<<');
//   });
// });

// Ex.2 simple chat ======================
// io.on('connection', (socket) => {
//   socket.on('message', (msg) => {
//     io.emit('message', msg);
//   });
// });

// Ex.3 chat rooms ===================
const nodeNameSpace = io.of('/nodeNameSpace');

nodeNameSpace.on('connection', (socket) => {
  socket.on('join', (data) => {
    socket.join(data.room);

    const msg = `${data.nick ? '' : 'New user '}joined ${data.room} room`;

    nodeNameSpace.in(data.room).emit('message', { msg, nick: data.nick });
  });

  socket.on('message', (data) => {
    nodeNameSpace.in(data.room).emit('message', { msg: data.msg, nick: data.nick });
  });
});

export default server;
