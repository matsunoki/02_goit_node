import { Email, userService } from '../services/index.js';
import { catchAsync } from '../utils/index.js';

export const signup = catchAsync(async (req, res) => {
  const { user, token } = await userService.signup(req.body);

  try {
    const url = `${req.protocol}://${req.get('host')}/home`;

    await new Email(user, url).sendHello();
  } catch (err) {
    console.log(err);
  }

  res.status(201).json({
    msg: 'Success!',
    user,
    token,
  });
});

export const login = catchAsync(async (req, res) => {
  const { user, token } = await userService.login(req.body);

  res.status(200).json({
    msg: 'Success!',
    user,
    token,
  });
});

export const forgotPassword = catchAsync(async (req, res) => {
  // validate req.body (email?)
  const user = await userService.getUserByEmail(req.body.email);

  if (!user) return res.status(200).json({ msg: 'Password reset sent by email..' });

  const otp = user.createPasswordResetToken();

  await user.save();

  console.log('>>>>>>>>> SEND TO EMAIL >>>>>>>>>>>>>>');
  console.log({ otp });
  console.log('<<<<<<<<<<<<<<<<<<<<<<<');

  // try {
  //   const emailTransporter = nodemailer.createTransport({
  //     // service: 'Gmail'
  //     host: serverConfig.mailtrapHost,
  //     port: serverConfig.mailtrapPort,
  //     auth: {
  //       user: serverConfig.mailtrapUser,
  //       pass: serverConfig.mailtrapPass,
  //     },
  //   });

  //   const emailConfig = {
  //     from: 'Todos App Admin <admin@example.com>',
  //     to: 'test@example.com',
  //     subject: 'Password reset testing',
  //     html: '<h1>Hello!!</h1>',
  //     text: 'Text version',
  //   };

  //   await emailTransporter.sendMail(emailConfig);
  // } catch (err) {
  //   console.log(err);
  // }

  try {
    // const resetUrl = https://myFrontend/resetPassword/7t8549tc58bgdvhtevgtregctec
    const resetUrl = `${req.protocol}://${req.get('host')}/api/v1/reset-password/${otp}`;

    await new Email(user, resetUrl).sendPasswordReset();
  } catch (err) {
    user.passwordResetToken = undefined;
    user.passwordResetTokenExp = undefined;

    await user.save();
  }

  res.status(200).json({ msg: 'Password reset sent by email..' });
});

export const restorePassword = catchAsync(async (req, res) => {
  // must validate password with regexp
  await userService.restorePassword(req.params.otp, req.body.password);

  res.status(200).json({
    msg: 'Success!',
  });
});
