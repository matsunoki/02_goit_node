import { userNameHandler } from '../userNameHandler.js';
// const userNameHandler = require('../userNameHandler.js');

// Simple test
// const buggyCalc = (a, b) => (a + b === 9 ? 10 : a + b);
const buggyCalc = (a, b) => a + b;

describe('Calc test example', () => {
  // test 1
  test('calc 1 + 1 = 2', () => {
    expect(buggyCalc(1, 1)).toBe(2);
  });

  // test 2
  it('should should return 9', () => {
    expect(buggyCalc(4, 5)).toBe(9);
  });
});

// User name handler tests
const testingData = [
  { input: 'Jimi Hendrix', output: 'Jimi Hendrix' },
  { input: 'jimi hendrix', output: 'Jimi Hendrix' },
  { input: 'jimi Hendrix', output: 'Jimi Hendrix' },
  { input: '   Jimi  hendriX ', output: 'Jimi Hendrix' },
  { input: 'Jimi_Hendrix', output: 'Jimi Hendrix' },
  { input: 'jimi.hendrix', output: 'Jimi Hendrix' },
  { input: 'jimi@hend@rix', output: 'Jimi Hend Rix' },
  { input: '_jimi * hendrix', output: 'Jimi Hendrix' },
  { input: 'jimi hèndrix__', output: 'Jimi Hendrix' },
  { input: 'jimi中村hèndrix__', output: 'Jimi Hendrix' },
  { input: 'jimi de Hèndrix__', output: 'Jimi De Hendrix' },
  { input: '中村哲二', output: '' },
  { input: undefined, output: '' },
  { input: null, output: '' },
  { input: true, output: '' },
];

describe('User name handler tests', () => {
  //   it('should returns "Jimi Hedrix" 1', () => {
  //     expect(userNameHandler(testingData[0].input)).toBe(testingData[0].output);
  //   });

  //   it('should returns "Jimi Hedrix" 2', () => {
  //     expect(userNameHandler(testingData[1].input)).toBe(testingData[1].output);
  //   });

  test('all user name input cases', () => {
    for (const item of testingData) {
      expect(userNameHandler(item.input)).toBe(item.output);
    }
  });
});
