import { Todo } from '../models/todoModel.js';
import { userRoles } from '../constants/userRoles.js';
import { HttpError } from '../utils/httpError.js';

export const createTodo = (todoData, owner) => {
  const { title, description, due } = todoData;

  return Todo.create({
    title,
    description,
    due,
    owner,
  });
};

export const getTodos = async (query, currentUser) => {
  // const todos = await Todo.find().populate('owner');
  //   const todos = await Todo.find().populate({ path: 'owner', select: 'name role email' });

  //   const filer = { title: { $regex: 'search', $options: 'i' } };
  //   const todos = await Todo
  //     .find()
  //     .populate({ path: 'owner', select: 'name role' })
  //     .sort('-title')
  //     .skip(9)
  //     .limit(3);

  // SEARCH FEATURE ================================
  const findOptions = query.search
    ? {
        $or: [
          { title: { $regex: query.search, $options: 'i' } },
          { description: { $regex: query.search, $options: 'i' } },
        ],
      }
    : {};

  if (currentUser.role === userRoles.USER) {
    if (query.search) {
      for (const findOption of findOptions.$or) {
        findOption.owner = currentUser;
      }
    }

    if (!query.search) findOptions.owner = currentUser;
  }

  // INIT DB QUERY ========================
  const todoQuery = Todo.find(findOptions).populate({ path: 'owner', select: 'name role' });

  // SORTING FEATURE =========================
  // order = 'ASC' | 'DESC'
  // todoQuery.sort('title' | '-title')
  todoQuery.sort(`${query.order === 'DESC' ? '-' : ''}${query.sort ?? 'title'}`);

  // PAGINATION FEATURE =======================
  // page 1 = limit 5, skip 0
  // page 2 = limit 5, skip 5
  // page 3 = limit 5, skip 10

  const page = query.page ? +query.page : 1;
  const limit = query.limit ? +query.limit : 5;
  const docsToSkip = (page - 1) * limit;

  todoQuery.skip(docsToSkip).limit(limit);

  const todos = await todoQuery;
  const total = await Todo.countDocuments(findOptions);

  return { todos, total };
};

export const getTodo = async (id, owner) => {
  const todo = await Todo.findById(id);

  if (!todo || (owner.role === userRoles.USER && todo.owner.toString() !== owner.id)) {
    throw new HttpError(404, 'Not found..');
  }

  return todo;
};
