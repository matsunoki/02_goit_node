import nodemailer from 'nodemailer';
import pug from 'pug';
import { convert } from 'html-to-text';
import path from 'path';

import { serverConfig } from '../configs/serverConfig.js';

export class Email {
  constructor(user, url) {
    this.to = user.email;
    this.name = user.name;
    this.url = url;
    this.from = serverConfig.emailFrom;
  }

  _initTransporter() {
    // production: MAILGUN
    // development: MAILTRAP

    const transportConfig = serverConfig.environment === 'production'
      ? {
        host: serverConfig.mailgunHost,
        port: serverConfig.mailgunPort,
        auth: {
          user: serverConfig.mailgunUser,
          pass: serverConfig.mailgunPass,
        },
      }
      : {
        host: serverConfig.mailtrapHost,
        port: serverConfig.mailtrapPort,
        auth: {
          user: serverConfig.mailtrapUser,
          pass: serverConfig.mailtrapPass,
        },
      };

    return nodemailer.createTransport(transportConfig);
  }

  async _send(template, subject) {
    const html = pug.renderFile(path.join(process.cwd(), 'views', 'email', `${template}.pug`), {
      name: this.name,
      url: this.url,
      subject,
    });

    const emailConfig = {
      from: this.from,
      to: this.to,
      subject,
      html,
      text: convert(html),
    };

    await this._initTransporter().sendMail(emailConfig);
  }

  async sendHello() {
    await this._send('hello', 'Welcome email');
  }

  async sendPasswordReset() {
    await this._send('restorePass', 'Password reset instruction');
  }
}

// https://github.com/leemunroe/responsive-html-email-template/tree/master
