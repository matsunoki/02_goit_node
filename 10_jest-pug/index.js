import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import mongoose from 'mongoose';

import './configs/dotenvConfig.js';
import { authRouter, todoRouter, userRouter, viewRouter } from './routes/index.js';
import { serverConfig } from './configs/serverConfig.js';
import { globalErrorHandler } from './controllers/errorController.js';

const app = express();

mongoose
  .connect(serverConfig.mongoUrl)
  .then(() => {
    console.log('Mongo DB connected..');
  })
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });

// MIDDLEWARES ===========================
if (process.env.NODE_ENV === 'development') app.use(morgan('dev'));
// app.use(morgan('dev'));

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

// Setup PUG template engine
app.set('view engine', 'pug');
app.set('views', 'views');

// global custom middleware
app.use((req, res, next) => {
  console.log('Hello from middleware!!!!');

  req.time = new Date().toLocaleString('uk-UA');

  next();
});

// CONTROLLERS ===========================
app.get('/ping', (req, res) => {
  // res.send('<p>Hello from server!!!</p>');
  // res.sendStatus(201);
  res.status(200).json({
    msg: 'pong!!!',
  });
});

// ROUTES ====================
const pathPrefix = '/api/v1';

app.use(`${pathPrefix}/auth`, authRouter);
app.use(`${pathPrefix}/todos`, todoRouter);
app.use(`${pathPrefix}/users`, userRouter);
app.use('/', viewRouter);

// Handle not found error
app.all('*', (req, res) => {
  res.status(404).json({
    msg: 'Oops! Resource not found!',
  });
});

// Global error handler
app.use(globalErrorHandler);

// SERVER INIT ===============================
const { port } = serverConfig;

export default app.listen(port, () => {
  console.log(`Server is up and running on port ${port}`);
});
