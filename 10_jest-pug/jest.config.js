export default {
  transform: {
    '^.+\\.m?js$': 'babel-jest',
  },
  testTimeout: 20000,
};
