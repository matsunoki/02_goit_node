import request from 'supertest';
import mongoose from 'mongoose';

import app from '../../index.js';
import { userService } from '../../services/index.js';

const userId = new mongoose.Types.ObjectId().toString();

const userPayload = {
  _id: userId,
  email: 'jimi@example.com',
  name: 'Jimi Hendrix',
};

describe('test POST /auth/login', () => {
  beforeAll(() => {
    console.log('BEFORE ALL');

    jest
      .spyOn(userService, 'login')
      .mockReturnValue({ user: userPayload, token: 'jwt.token.string' });
  });

  // beforeEach(() => console.log('BEFORE EACH'));
  // afterEach(() => console.log('AFTER EACH'));
  // afterAll(() => console.log('AFTER ALL'));

  it('should return unauthorized error - 1', async () => {
    const testData = {
      email: 'jimi@example.com',
      password: 'Pass234',
    };

    const res = await request(app).post('/api/v1/auth/login').send(testData);

    expect(res.statusCode).toBe(401);
  });

  it('should return unauthorized error - 2', async () => {
    const testData = {
      email: 'jimi@example.com',
      passord: 'Pass&1234',
    };

    const res = await request(app).post('/api/v1/auth/login').send(testData);

    expect(res.statusCode).toBe(401);
  });

  it('should return a user object and a token', async () => {
    const correctUserData = {
      email: 'jimi@example.com',
      password: 'Pass_1234',
    };

    const res = await request(app).post('/api/v1/auth/login').send(correctUserData);

    expect(res.statusCode).toBe(200);
    expect(res.body).toEqual(
      expect.objectContaining({
        token: expect.any(String),
        user: expect.any(Object),
      })
    );
  });
});
