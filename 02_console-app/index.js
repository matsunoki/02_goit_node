// console.log(global);

// process.env.NODE_MODE = 'development';
// console.log(process.env); // environment variables
// console.log(process.argv); // arguments array
// console.log(process.cwd()); // current working directory
// process.exit(); // stop script execution

// console.log(__dirname);
// console.log(__filename);

const { program } = require('commander');
const fs = require('fs').promises;
const readline = require('readline');
require('colors');

// SETUP ARGUMENT
// <type> - show error if no arg
// 2nd arg - description
// 3rd arg - default value
// using args: 'node index.js -f <name of the log file>'
program.option('-f, --file [type]', 'file for saving game logs', 'default.log');

// parse command line arguments
program.parse(process.argv);

// create interaction interface
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// simple example of using readline
// rl.on('line', (txt) => {
//   console.log('>>>>>>>>>>>>>>>>>>>>>>>');
//   console.log({ txt });
//   console.log('<<<<<<<<<<<<<<<<<<<<<<<');

//   process.exit();
// });

let counter = 0;
const mind = Math.ceil(Math.random() * 10); // Random number in range from 1 to 10
const logFile = program.opts().file;

/**
 * Logger to write game results into the log file
 * @param {string} msg - message to log
 * @param {string} logFile - file to log
 * @returns {Promise<void>}
 */
const logger = async (msg, logFile) => {
  try {
    await fs.appendFile(logFile, `${new Date().toLocaleString('uk-UA')}: ${msg}\n`);

    console.log(msg.bgWhite.brightMagenta);
    console.log(`Saved game results to the log file: ${logFile}}`.yellow);
  } catch (err) {
    console.log(`Something went very wrong: ${err.message}`.red);
  }
};

/**
 * Simple input value validator
 * @author Sergii
 * @category validators
 *
 * @param {number} num - input value
 * @returns {boolean}
 */
const isValid = (num) => {
  if (!Number.isNaN(num) && num > 0 && num <= 10 && num % 1 === 0) return true;

  if (Number.isNaN(num)) console.log('Please, enter a number only..'.red);
  if (num % 1 !== 0) console.log('Please, enter a whole number only..'.red);
  if (num < 1 || num > 10) console.log('Number should be between 1 and 10'.red);

  return false;
};

/**
 * Main game cycle
 */
const game = () => {
  rl.question('Please, enter any whole number from 1 to 10..\n'.green, (val) => {
    // const num = Number(val);
    const num = +val;

    // validate the number
    if (!isValid(num)) return game();

    // counter = counter + 1;
    // counter += 1;
    // ++counter;
    counter++;

    if (num !== mind) {
      console.log('Oh no!! Try again..'.red);

      return game();
    }

    logger(`Congrats!! You've guessed the number in ${counter} step(s)`, logFile);

    // process.exit();
    rl.close();
  });
};

game();
