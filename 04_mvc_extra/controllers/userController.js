import { v4 } from 'uuid';
import { promises as fs } from 'fs';

import { catchAsync, HttpError } from '../utils/index.js';
import { createUserDataValidator } from '../utils/userValidators.js';

export const createUser = catchAsync(async (req, res) => {
  const { value, error } = createUserDataValidator(req.body);

  if (error) throw new HttpError(400, 'Invalid user data..');

  const { name, year } = value;

  const newUser = {
    id: v4(),
    name,
    year,
  };

  // Save user to the "DB"
  const usersDB = await fs.readFile('data.json');

  const users = JSON.parse(usersDB);

  users.push(newUser);

  await fs.writeFile('data.json', JSON.stringify(users));

  res.status(201).json({
    msg: 'Success!',
    user: newUser,
  });
});

export const getUsersList = catchAsync(async (req, res) => {
  const usersDB = await fs.readFile('data.json');

  const users = JSON.parse(usersDB);

  res.status(201).json({
    msg: 'Success!',
    users,
  });
});

export const getOneUser = (req, res) => {
  res.status(201).json({
    msg: 'Success!',
    user: req.user,
    // time: req.time,
  });
};
