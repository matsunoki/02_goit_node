import { promises as fs } from 'fs';

import { catchAsync, HttpError } from '../utils/index.js';

export const checkUserId = catchAsync(async (req, res, next) => {
  const { id } = req.params;

  // if (id.length < 10) return res.status(400).json({ msg: 'Failed..' });
  if (id.length < 10) throw new HttpError(400, 'Bad id..');

  const usersDB = await fs.readFile('data.json');

  const users = JSON.parse(usersDB);
  const user = users.find((item) => item.id === req.params.id);

  if (!user) throw new HttpError(404, 'User not found..');

  req.user = user;

  next();
});
