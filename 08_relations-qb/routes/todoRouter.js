import { Router } from 'express';

import { todoController } from '../controllers/index.js';
import { authMiddleware } from '../middlewares/index.js';

const router = Router();

router.use(authMiddleware.protect);
router.post('/', todoController.createTodo);
router.get('/', todoController.getTodos);

export { router };
