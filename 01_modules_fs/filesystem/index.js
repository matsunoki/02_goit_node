const fs = require('fs').promises;
const path = require('path');

// IIFE
(async () => {
  try {
    // READ text file from file syste ================
    // /user/project/example.txt
    // D:\\user\project\example.txt
    const pathToTextFile = path.join('files', 'texts', 'example.txt');

    const readResult = await fs.readFile(pathToTextFile);

    // console.log(readResult.toString());

    const filesDir = 'files';

    const listDirectoryContent = await fs.readdir(filesDir);
    const stat = await fs.lstat(filesDir);

    // await fs.appendFile(pathToTextFile, '\nNEW LINE!!!!');

    // READ json file ==================================
    const pathToJson = path.join('files', 'data.json');

    const readJsonResult = await fs.readFile(pathToJson);

    const dataArr = JSON.parse(readJsonResult);
    dataArr.push({ name: 'Jimi', year: 1946 });

    await fs.writeFile(pathToJson, JSON.stringify(dataArr));
  } catch (err) {
    console.log(err);
  }
})();
