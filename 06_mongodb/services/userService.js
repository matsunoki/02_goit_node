import { Types } from 'mongoose';
import { User } from '../models/userModel.js';
import { HttpError } from '../utils/httpError.js';

/**
 * Create user service.
 * @param {Object} userData
 * @returns {Promise<User>}
 *
 * @author Sergii
 * @category services
 */
export const createUser = async (userData) => {
  const newUser = await User.create(userData);

  // const newUser = User(userData);

  // await newUser.save();

  newUser.password = undefined;

  return newUser;
};

// export const getAllUsers = async () => {
//   // const users = await User.find().select('+password');
//   // const users = await User.find().select('-email');
//   // const users = await User.find().select('name email');

//   const users = await User.find();

//   return users;
// };
export const getAllUsers = () => User.find();

export const getUserById = (id) => User.findById(id);

export const updateUser = async (id, userData) => {
  // const updatedUser = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });
  const user = await User.findById(id);

  Object.keys(userData).forEach((key) => {
    user[key] = userData[key];
  });

  return user.save();
};

export const hideUser = (id) => User.findByIdAndDelete(id);

export const checkUserExists = async (filter) => {
  const userExists = await User.exists(filter);

  if (userExists) throw new HttpError(409, 'User already exists..');
};

export const checkUserId = async (id) => {
  const isIdValid = Types.ObjectId.isValid(id);

  if (!isIdValid) throw new HttpError(404, 'User not found..');

  const userExists = await User.exists({ _id: id });
  // const userExists = await User.findById(id).select('_id');

  if (!userExists) throw new HttpError(404, 'User not found..');
};
