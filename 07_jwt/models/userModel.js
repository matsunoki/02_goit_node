import { model, Schema } from 'mongoose';
import { compare, genSalt, hash } from 'bcrypt';

import { userRoles } from '../constants/index.js';

const userSchema = new Schema(
  {
    // name: String,
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      select: false,
    },
    year: Number,
    role: {
      type: String,
      enum: Object.values(userRoles),
      default: userRoles.USER,
    },
    hidden: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

// MONGOOSE HOOKS
// userSchema.pre(/^find/, () => {
//   console.log('ALL FIND HOOKS');
// });
// userSchema.pre('find', () => {
//   console.log('FIND HOOK');
// });

// Pre save hook fires on "save" and "create" methods.
userSchema.pre('save', async function(next) {
  if (!this.isModified('password')) return next();

  const salt = await genSalt(10);
  this.password = await hash(this.password, salt);

  // const passwordIsValid = await bcrypt.compare('Pass_1234', hashedPassword);

  next();
});

// CUSTOM METHODS
userSchema.methods.checkPassword = (candidate, passwordHash) => compare(candidate, passwordHash);

const User = model('User', userSchema);

export { User };
