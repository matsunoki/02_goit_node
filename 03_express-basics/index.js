import express from 'express';
import cors from 'cors';
import { v4 } from 'uuid';
import { promises as fs } from 'fs';

const app = express();

// MIDDLEWARES ===========================
app.use(express.json());
app.use(cors());

// global custom middleware
app.use((req, res, next) => {
  console.log('Hello from middleware!!!!');

  req.time = new Date().toLocaleString('uk-UA');

  next();
});

app.use('/users/:id', async (req, res, next) => {
  try {
    const { id } = req.params;

    if (id.length < 10) {
      return res.status(400).json({
        msg: 'Failed..',
      });
    }

    const usersDB = await fs.readFile('data.json');

    const users = JSON.parse(usersDB);
    const user = users.find((item) => item.id === req.params.id);

    if (!user) {
      return res.status(404).json({
        msg: 'User not found..',
      });
    }

    req.user = user;

    next();
  } catch (err) {
    console.log(err);
  }
});

// CONTROLLERS ===========================
app.get('/ping', (req, res) => {
  // res.send('<p>Hello from server!!!</p>');
  // res.sendStatus(201);
  res.status(200).json({
    msg: 'pong!!!',
  });
});

/**
 * HTTP methods ================
 * POST, GET, PUT, PATCH, DELETE
 *
 * REST API (CRUD operations)
 * POST         /users            - user creation
 * GET          /users            - get users list
 * GET          /users/<userID>   - get one user
 * PATCH(PUT)   /users/<userID>   - update one user
 * DELETE       /users/<userID>   - delete one user
 */

app.post('/users', async (req, res) => {
  try {
    const { name, year } = req.body;

    // TODO: req.body validation!!!!!!

    const newUser = {
      id: v4(),
      name,
      year,
    };

    // Save user to the "DB"
    const usersDB = await fs.readFile('data.json');

    const users = JSON.parse(usersDB);

    users.push(newUser);

    await fs.writeFile('data.json', JSON.stringify(users));

    res.status(201).json({
      msg: 'Success!',
      user: newUser,
    });
  } catch (err) {
    console.log(err);
  }
});

app.get('/users', async (req, res) => {
  try {
    const usersDB = await fs.readFile('data.json');

    const users = JSON.parse(usersDB);

    res.status(201).json({
      msg: 'Success!',
      users,
    });
  } catch (err) {
    console.log(err);
  }
});

app.get('/users/:id', (req, res) => {
  res.status(201).json({
    msg: 'Success!',
    user: req.user,
    time: req.time,
  });
});

// app.patch('/users/:id', (req, res) => {});
// app.delete('/users/:id', (req, res) => {});

// SERVER INIT ===============================
const port = 3001;

app.listen(port, () => {
  console.log(`Server is up and running on port ${port}`);
});
